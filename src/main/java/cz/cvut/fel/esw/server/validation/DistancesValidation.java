package cz.cvut.fel.esw.server.validation;

import lombok.Data;

/**
 * DistancesValidation class represents a validation for checking the validity of distances.
 */
@Data
public class DistancesValidation implements Validation {

    private Long oldDistance;
    private Long newDistance;
    private boolean isValid;

    /**
     * Constructs a new instance of DistancesValidation.
     *
     * @param oldDistance the old distance to validate against
     * @param newDistance the new distance to validate
     */
    public DistancesValidation(Long oldDistance, Long newDistance) {
        this.oldDistance = oldDistance;
        this.newDistance = newDistance;
    }

    /**
     * Validates the distances based on multiple conditions.
     */
    public void validateDistances() {
        AggregatedValidation validation = new AggregatedValidation()
                .add(this::oldDistanceIsNull)
                .add(this::isNewDistanceLowerThatOldDistance);

        isValid = validation.validateWithOrOperator();
    }

    /**
     * Checks if the old distance is null.
     *
     * @return true if the old distance is null, false otherwise
     */
    private boolean oldDistanceIsNull() {
        return this.oldDistance == null;
    }

    /**
     * Checks if the new distance is lower than the old distance.
     *
     * @return true if the new distance is lower than the old distance, false otherwise
     */
    private boolean isNewDistanceLowerThatOldDistance() {
        return this.newDistance < this.oldDistance;
    }

    /**
     * Checks if the distance validation is valid.
     *
     * @return true if the distance validation is valid, false otherwise
     */
    @Override
    public boolean isValid() {
        return isValid;
    }
}
