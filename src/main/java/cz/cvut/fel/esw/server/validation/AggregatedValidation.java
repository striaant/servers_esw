package cz.cvut.fel.esw.server.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * AggregatedValidation class represents a collection of validations that can be aggregated and checked for validity.
 */
public class AggregatedValidation {

    private final List<Supplier<Boolean>> validations;

    /**
     * Constructs a new instance of AggregatedValidation.
     */
    public AggregatedValidation() {
        this.validations = new ArrayList<>();
    }

    /**
     * Adds a validation to the collection.
     *
     * @param validation the validation to add
     * @return the AggregatedValidation instance
     */
    public AggregatedValidation add(Supplier<Boolean> validation) {
        validations.add(validation);
        return this;
    }

    /**
     * Validates all the validations in the collection using the AND operator.
     *
     * @return true if all validations are valid, false otherwise
     */
    public boolean validate() {
        for (Supplier<Boolean> validation : validations) {
            if (!validation.get()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validates all the validations in the collection using the OR operator.
     *
     * @return true if any of the validations is valid, false if none of the validations is valid
     */
    public boolean validateWithOrOperator() {
        for (Supplier<Boolean> validation : validations) {
            if (validation.get()) {
                return true;
            }
        }
        return false;
    }
}
