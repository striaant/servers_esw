package cz.cvut.fel.esw.server.graph;


/**
 * Represents a holder class for the source vertex (from) and the target vertex (to) in a graph edge.
 * It is used to store the from-to mapping in the graph calculations.
 */
public record FromToHolder(Long from, Integer edge) { }
