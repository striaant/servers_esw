package cz.cvut.fel.esw.server.tcp;

import cz.cvut.fel.esw.server.proto.Request;
import cz.cvut.fel.esw.server.proto.Response;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Defines the interface for a TCP server.
 */
public interface Tcp {

    /**
     * Runs the TCP server.
     *
     * @throws IOException if an I/O error occurs
     */
    void run() throws IOException;

    /**
     * Parses the incoming message from the input stream.
     *
     * @param inputStream the input stream to read the message from
     * @return the parsed message as a byte array
     * @throws IOException if an I/O error occurs
     */
    byte[] parseMessage(InputStream inputStream) throws IOException;

    /**
     * Handles the incoming request and writes the response to the output stream.
     *
     * @param request the incoming request
     * @param os      the output stream to write the response to
     * @throws IOException if an I/O error occurs
     */
    void handleRequest(Request request, OutputStream os) throws IOException;

    /**
     * Writes the response to the output stream.
     *
     * @param response the response to write
     * @param os       the output stream to write the response to
     * @throws IOException if an I/O error occurs
     */
    void write(Response response, OutputStream os) throws IOException;
}
