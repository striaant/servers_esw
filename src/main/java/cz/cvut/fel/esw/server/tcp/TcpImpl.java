package cz.cvut.fel.esw.server.tcp;

import cz.cvut.fel.esw.server.constants.Constants;
import cz.cvut.fel.esw.server.graph.GraphFacade;
import cz.cvut.fel.esw.server.proto.Location;
import cz.cvut.fel.esw.server.proto.Request;
import cz.cvut.fel.esw.server.proto.Response;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

@RequiredArgsConstructor
public class TcpImpl implements Tcp {
    private final int port;
    private final ExecutorService executors = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private GraphFacade graph = new GraphFacade();

    private final Map<Request.MsgCase, Function<Request, Response>> messageToAction = new HashMap<>() {{
        put(Request.MsgCase.WALK, req -> {
            graph.walk(req.getWalk());
            return Response.newBuilder().setStatus(Response.Status.OK).build();
        });
        put(Request.MsgCase.ONETOONE, req -> {
            Location origin = req.getOneToOne().getOrigin();
            long res = graph.oneToOne(origin, req.getOneToOne().getDestination());
            System.out.printf("one to one: " + res + "\n");
            return Response.newBuilder().setStatus(Response.Status.OK).setShortestPathLength(res).build();
        });
        put(Request.MsgCase.ONETOALL, req -> {
            long res = graph.oneToAll(req.getOneToAll().getOrigin());
            System.out.printf("one to all: " + res + "\n");
            return Response.newBuilder().setStatus(Response.Status.OK).setTotalLength(res).build();
        });
        put(Request.MsgCase.RESET, req -> {
            graph = new GraphFacade();
            return Response.newBuilder().setStatus(Response.Status.OK).build();
        });
    }};

    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            while (true) {
                Socket socket = serverSocket.accept();
                executors.submit(() -> {
                    try {
                        while (true) {
                            byte[] serializedMessage = parseMessage(socket.getInputStream());
                            if (serializedMessage == null) {
                                socket.close();
                                break;
                            }
                            handleRequest(Request.parseFrom(serializedMessage), socket.getOutputStream());
                        }
                    } catch (IOException e) {
                    }
                });
            }
        } catch (IOException e) {
        }
    }

    @Override
    public byte[] parseMessage(InputStream inputStream) throws IOException {
        byte[] size = new byte[Constants.SKIP_BYTES];
        if (inputStream.read(size) == -1) {
            return null;
        }
        int serializedSize = ByteBuffer.wrap(size).order(ByteOrder.BIG_ENDIAN).getInt();
        byte[] message = new byte[serializedSize];
        int bytesRead = 0;
        while (bytesRead < serializedSize) {
            int count = inputStream.read(message, bytesRead, serializedSize - bytesRead);
            if (count == -1) {
                return null;
            }
            bytesRead += count;
        }
        return message;
    }


    @Override
    public void handleRequest(Request request, OutputStream os) throws IOException {
        Response response = messageToAction.get(request.getMsgCase()).apply(request);
        write(response,os);
    }

    @Override
    public void write(Response response, OutputStream os) throws IOException {
        var responseInByteArray = response.toByteArray();
        var respLength = ByteBuffer.allocate(Constants.SKIP_BYTES).putInt(responseInByteArray.length).array();
        os.write(respLength);
        os.write(responseInByteArray);
    }

}



