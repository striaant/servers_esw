package cz.cvut.fel.esw.server.graph;

import cz.cvut.fel.esw.server.constants.Constants;
import cz.cvut.fel.esw.server.proto.Location;
import cz.cvut.fel.esw.server.proto.Walk;
import cz.cvut.fel.esw.server.validation.CoordinateValidation;
import cz.cvut.fel.esw.server.validation.DistancesValidation;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Represents a graph with locations and edges.
 * Provides methods to manipulate and calculate properties of the graph.
 */
@Data
@AllArgsConstructor
public class Graph {

    private final ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Edge>> adjList;
    private final List<Location> locationsList;
    private final ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Integer>> mapToVertexId;
    private int locations;

    /**
     * Adds a walk to the graph based on the given Walk object.
     *
     * @param walk the walk to be added
     */
    public synchronized void walk(Walk walk) {
        List<Location> locationList = walk.getLocationsList();
        if (locationList.isEmpty()) {
            return;
        }

        var start = getId(locationList.get(0));

        for (int i = 1; i < locationList.size(); i++) {
            var current = locationList.get(i);
            var end = getId(current);

            var tempAdjList = adjList.computeIfAbsent(start, k -> new ConcurrentHashMap<>());
            var edge = tempAdjList.get(end);
            if (edge == null) {
                edge = new Edge(0, 0);
                tempAdjList.put(end, edge);
            }

            var walkLen = walk.getLengths(i - 1);
            edge.addWalk();
            edge.extendLength(walkLen);
            start = end;
        }
    }

    /**
     * Calculates the shortest path between two locations in the graph.
     *
     * @param start the starting location
     * @param end   the ending location
     * @return the shortest path length between the start and end locations
     */
    public long calculate(Location start, Location end) {
        Integer startId = getId(start);
        Integer endId = getId(end);
        Map<Integer, Long> dist = new ConcurrentHashMap<>();
        dist.put(startId, 0L);
        PriorityQueue<FromToHolder> pq = new PriorityQueue<>(Comparator.comparingLong(FromToHolder::from));
        pq.offer(new FromToHolder(0L, startId));

        long resultLen = 0;
        Map<Integer, Boolean> isVisited = new ConcurrentHashMap<>();

        while (!pq.isEmpty()) {
            FromToHolder fromToHolder = pq.poll();
            int current = fromToHolder.edge();

            if (isVisited.containsKey(current)) {
                continue;
            }

            isVisited.put(current, true);
            resultLen += dist.get(current);

            if (!adjList.containsKey(current)) {
                continue;
            }

            ConcurrentHashMap<Integer, Edge> currentAdjList = adjList.get(current);
            for (ConcurrentHashMap.Entry<Integer, Edge> entry : currentAdjList.entrySet()) {
                int destination = entry.getKey();
                long newDistance = dist.get(current) + entry.getValue().getWeight();

                if (!adjList.containsKey(destination)) {
                    adjList.computeIfAbsent(destination, k -> new ConcurrentHashMap<>());
                    updateDist(dist, pq, destination, newDistance);
                } else {
                    if (!isVisited.containsKey(destination) && validateDistances(dist.get(destination), newDistance)) {
                        updateDist(dist, pq, destination, newDistance);
                    }
                }
            }
        }

        return (end == null) ? resultLen : dist.get(endId);
    }

    /**
     * Updates the distance map and priority queue with the new distance for a given destination.
     *
     * @param distMap    the distance map
     * @param pq         the priority queue
     * @param destination the destination vertex
     * @param newDist    the new distance to the destination
     */
    private void updateDist(Map<Integer, Long> distMap, PriorityQueue<FromToHolder> pq, int destination, long newDist) {
        distMap.put(destination, newDist);
        pq.offer(new FromToHolder(newDist, destination));
    }

    /**
     * Gets the ID of a location in the graph.
     * If the location does not exist, it is added to the graph.
     *
     * @param location the location
     * @return the ID of the location
     */
    public Integer getId(Location location) {
        if (location == null) {
            return null;
        }
        int x = location.getX() / Constants.ADMISSIBLE_ERROR;
        int y = location.getY() / Constants.ADMISSIBLE_ERROR;

        Map<Integer, Integer> vertexMap = mapToVertexId.get(x);
        if (vertexMap != null) {
            Integer vertexId = vertexMap.get(y);
            if (vertexId != null && validateCoordinates(x, y, location)) {
                return vertexId;
            }
        }

        for (int[] direction : Constants.DIRECTIONS) {
            int possibleX = x + direction[0];
            int possibleY = y + direction[1];

            Map<Integer, Integer> neighborVertexMap = mapToVertexId.get(possibleX);
            if (neighborVertexMap != null) {
                Integer neighborVertexId = neighborVertexMap.get(possibleY);
                if (neighborVertexId != null && validateCoordinates(possibleX, possibleY, location)) {
                    return neighborVertexId;
                }
            }
        }

        int key = location.getX() / Constants.ADMISSIBLE_ERROR;
        int value = location.getY() / Constants.ADMISSIBLE_ERROR;
        mapToVertexId.computeIfAbsent(key, k -> new ConcurrentHashMap<>()).put(value, locations);
        locationsList.add(location);
        return locations++;
    }

    /**
     * Validates the coordinates of a location and determines if it is valid for adding to the graph.
     *
     * @param possibleX  the possible x-coordinate
     * @param possibleY  the possible y-coordinate
     * @param location   the location
     * @return true if the coordinates are valid, false otherwise
     */
    private boolean validateCoordinates(int possibleX, int possibleY, Location location) {
        CoordinateValidation validation = new CoordinateValidation(possibleX, possibleY, location, mapToVertexId, locationsList);
        validation.validateCoordinates();
        return validation.isValid();
    }

    /**
     * Validates the distances between old and new values and determines if the new distance is valid.
     *
     * @param oldDist the old distance
     * @param newDist the new distance
     * @return true if the new distance is valid, false otherwise
     */
    private boolean validateDistances(Long oldDist, Long newDist) {
        DistancesValidation validation = new DistancesValidation(oldDist, newDist);
        validation.validateDistances();
        return validation.isValid();
    }
}
