package cz.cvut.fel.esw.server.graph;

import lombok.Data;

/**
 * Represents an edge in a graph.
 * Provides methods to manipulate and calculate properties of the edge.
 */
@Data
public class Edge {
    private int length;
    private int walks;

    /**
     * Constructs a new Edge with the specified length and number of walks.
     *
     * @param length the length of the edge
     * @param walks  the number of walks taken on the edge
     */
    public Edge(int length, int walks) {
        this.length = length;
        this.walks = walks;
    }

    /**
     * Extends the length of the edge by the specified amount.
     *
     * @param length the additional length to be added
     */
    public void extendLength(int length) {
        this.length += length;
    }

    /**
     * Increments the number of walks taken on the edge by 1.
     */
    public void addWalk() {
        this.walks++;
    }

    /**
     * Calculates and returns the weight of the edge.
     * The weight is calculated as the length divided by the number of walks.
     *
     * @return the weight of the edge
     */
    public int getWeight() {
        return this.length / this.walks;
    }
}
