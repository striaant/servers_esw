package cz.cvut.fel.esw.server.graph;

import cz.cvut.fel.esw.server.proto.Location;
import cz.cvut.fel.esw.server.proto.Walk;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Facade class that provides simplified access to the underlying Graph class.
 * It encapsulates the graph manipulation and calculation operations.
 */
public class GraphFacade {

    private final ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Edge>> adjList;
    private final List<Location> locationsList;
    private final ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Integer>> mapToVertexId;
    private final Graph graph;

    /**
     * Constructs a new GraphFacade with an empty graph.
     */
    public GraphFacade() {
        this.adjList = new ConcurrentHashMap<>();
        this.locationsList = new ArrayList<>();
        this.mapToVertexId = new ConcurrentHashMap<>();
        this.graph = new Graph(this.adjList, this.locationsList, this.mapToVertexId, 0);
    }

    /**
     * Adds a walk to the graph based on the given Walk object.
     *
     * @param walk the walk to be added
     */
    public void walk(Walk walk) {
        this.graph.walk(walk);
    }

    /**
     * Calculates the shortest path length between two locations in the graph.
     *
     * @param start the starting location
     * @param end   the ending location
     * @return the shortest path length between the start and end locations
     */
    public long oneToOne(Location start, Location end) {
        return this.graph.calculate(start, end);
    }

    /**
     * Calculates the shortest path length between a starting location and all other locations in the graph.
     *
     * @param start the starting location
     * @return the shortest path length between the start location and all other locations
     */
    public long oneToAll(Location start) {
        return this.graph.calculate(start, null);
    }
}
