package cz.cvut.fel.esw.server.validation;

import cz.cvut.fel.esw.server.constants.Constants;
import cz.cvut.fel.esw.server.proto.Location;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * CoordinateValidation class represents a validation for checking the validity of coordinates in a graph.
 */
public class CoordinateValidation implements Validation {

    private final int possibleX;
    private final int possibleY;
    private final Location location;
    private boolean isValid;
    private final ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Integer>> mapToVertexId;
    private final List<Location> locationsList;

    /**
     * Constructs a new instance of CoordinateValidation.
     *
     * @param possibleX       the possible x-coordinate
     * @param possibleY       the possible y-coordinate
     * @param location        the location object to validate against
     * @param mapToVertexId   the mapping of coordinates to vertex IDs
     * @param locationsList   the list of locations
     */
    public CoordinateValidation(int possibleX,
                                int possibleY,
                                Location location,
                                ConcurrentHashMap<Integer, ConcurrentHashMap<Integer, Integer>> mapToVertexId,
                                List<Location> locationsList) {
        this.possibleX = possibleX;
        this.possibleY = possibleY;
        this.location = location;
        this.isValid = false;
        this.mapToVertexId = mapToVertexId;
        this.locationsList = locationsList;
    }

    /**
     * Checks if the coordinate validation is valid.
     *
     * @return true if the coordinate validation is valid, false otherwise
     */
    public boolean isValid() {
        return isValid;
    }

    /**
     * Validates the coordinates based on multiple conditions.
     */
    public void validateCoordinates() {
        AggregatedValidation validation = new AggregatedValidation()
                .add(this::possibleCoordinatesBiggerOrEqualZero)
                .add(this::coordinateExists)
                .add(this::euclideanDistanceLowerOrEqualAdmissibleError);

        isValid = validation.validate();
    }

    /**
     * Calculates the squared Euclidean distance between two points.
     *
     * @param point1 the first point
     * @param point2 the second point
     * @return the squared Euclidean distance between the two points
     */
    private long squaredEuclideanDistance(Location point1, Location point2) {
        long dx = point1.getX() - point2.getX();
        long dy = point1.getY() - point2.getY();
        return dx * dx + dy * dy;
    }

    /**
     * Checks if the possible coordinates are greater than or equal to zero.
     *
     * @return true if the possible coordinates are greater than or equal to zero, false otherwise
     */
    private boolean possibleCoordinatesBiggerOrEqualZero() {
        return possibleX >= 0 && possibleY >= 0;
    }

    /**
     * Checks if the Euclidean distance between the given location and the vertex from the possible coordinates
     * is lower than or equal to the admissible error constant.
     *
     * @return true if the Euclidean distance is lower than or equal to the admissible error, false otherwise
     */
    public boolean euclideanDistanceLowerOrEqualAdmissibleError() {
        return squaredEuclideanDistance(location, getVertexFromCoordinates(possibleX, possibleY)) <= Constants.SQUARE_ADMISSIBLE_ERROR;
    }

    /**
     * Checks if the coordinate exists in the mapToVertexId.
     *
     * @return true if the coordinate exists in the mapToVertexId, false otherwise
     */
    private boolean coordinateExists() {
        if (mapToVertexId.containsKey(possibleX)) {
            return mapToVertexId.get(possibleX).containsKey(possibleY);
        }
        return false;
    }

    /**
     * Retrieves the vertex from the coordinates.
     *
     * @param possibleX the x-coordinate
     * @param possibleY the y-coordinate
     * @return the vertex corresponding to the given coordinates
     */
    private Location getVertexFromCoordinates(int possibleX, int possibleY) {
        return locationsList.get(mapToVertexId.get(possibleX).get(possibleY));
    }
}
