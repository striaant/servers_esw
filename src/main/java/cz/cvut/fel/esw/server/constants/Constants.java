package cz.cvut.fel.esw.server.constants;

/**
 * Constants class contains static constant values used in the server application.
 */
public class Constants {
    /**
     * The admissible error value.
     */
    public static int ADMISSIBLE_ERROR = 500;

    /**
     * The square of the admissible error value.
     */
    public static int SQUARE_ADMISSIBLE_ERROR = 250000;

    /**
     * The array of directions for neighboring vertices.
     */
    public static final int[][] DIRECTIONS = {{0, 0}, {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};

    /**
     * The number of bytes to skip in the input stream.
     */
    public static final int SKIP_BYTES = 4;
}
