package cz.cvut.fel.esw.server;

import cz.cvut.fel.esw.server.tcp.TcpImpl;

import java.io.IOException;

/**
 * Main class represents the entry point of the server application.
 */
public class Main {
    /**
     * The main method starts the server by initializing and running the TCP implementation.
     *
     * @param args command-line arguments (not used)
     */
    public static void main(String[] args){
        TcpImpl tcp = new TcpImpl(1337);
        tcp.run();
    }
}
