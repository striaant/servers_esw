package cz.cvut.fel.esw.server.validation;

/**
 * Interface for validations.
 */
public interface Validation {

    /**
     * Checks if the validation is valid.
     *
     * @return true if the validation is valid, false otherwise
     */
    boolean isValid();
}
