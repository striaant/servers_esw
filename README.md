# Server Application Readme

[![Build Status](https://img.shields.io/badge/build-passing-brightgreen)](#)
[![License](https://img.shields.io/badge/license-MIT-blue)](LICENSE)

This readme provides instructions for compiling and running the server application, information about the used libraries, and a brief description of the algorithms and techniques employed to solve the challenges present in the task.

## Requirements

- Nix package manager (for Nix shell environment)

## Instructions

### Build and Run

To build and run the server application, please follow these steps:

1. Ensure that you have the Nix package manager installed on your system. If not, you can install it by following the instructions at [https://nixos.org/download.html](https://nixos.org/download.html).

2. Clone the repository or download the source code files.

3. Open a terminal and navigate to the root directory of the server application.

4. Run the following command to enter the Nix shell environment:

    `nix-shell`

   This command sets up the required development environment with the necessary dependencies.

5. Once inside the Nix shell, run the following command to compile the Java files:

    `mvn compile`
   
    This command compiles the Java source code and resolves any required dependencies.

6. Finally, run the following command to execute the server application:

    `mvn exec:java -Dexec.mainClass="cz.cvut.fel.esw.server.Main"`

   This command starts the server application and listens on the specified port (e.g., 1337).

7. You can now interact with the server application by sending requests over TCP.

### Usage

Once the server application is running, you can send requests to it over TCP. The server expects the requests to be in a specific format defined by the application's protocol.

Please refer to the documentation or specifications of the client application or protocol to understand how to communicate with the server and send valid requests.

## Used Libraries

The server application uses the following libraries:

- **Lombok**: Lombok is used for reducing boilerplate code by automatically generating getters, setters, constructors, and other common code. It simplifies the development process and improves code readability.

- **ConcurrentHashMap**: The `ConcurrentHashMap` class from the Java standard library is used for concurrent access to shared data structures in a multi-threaded environment. It provides thread-safe operations for efficient concurrent operations on the graph data structure.

- **PriorityQueue**: The `PriorityQueue` class from the Java standard library is used for implementing priority-based queue functionality. It is used in the graph algorithm to determine the next vertex to process based on the distances.

## Algorithms and Techniques

The server application implements the following algorithms and techniques to solve the challenges present in the task:

- **Graph Representation**: The application represents the graph using an adjacency list data structure. It utilizes a `ConcurrentHashMap` to store the edges between vertices efficiently.

- **Dijkstra's Algorithm**: The `calculate` method in the `Graph` class implements Dijkstra's algorithm to find the shortest path between two vertices in the graph. It maintains a priority queue (`PriorityQueue`) to select the next vertex with the minimum distance and updates the distances of adjacent vertices accordingly.

- **Coordinate Validation**: The `CoordinateValidation` class performs validation of coordinates to ensure they are within the expected range. It uses an `AggregatedValidation` class to combine multiple validation rules and determine the overall validity of the coordinates.

- **Distance Validation**: The `DistancesValidation` class validates distances to ensure
