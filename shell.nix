with (import <nixpkgs> {});
mkShell {
  buildInputs = [
    jdk17
    protobuf
    maven
  ];
}